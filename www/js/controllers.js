angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('SellCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, $http, Chats) {
    
    $http.get('http://localhost:3000/chats').
    success(function(data, status, headers, config) {
      Chats.add(data);
      $scope.chats = Chats.all();
      console.log("http get");
      console.log($scope.chats);
    }).
    error(function(data, status, headers, config) {
      // log error
    });

 // $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  }
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
